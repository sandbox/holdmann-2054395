Commerce Yandex fast order
=====================

This module adds "address from Yandex profile" button to 
customer shipping profile pane.
When its clicked, the order form will be automatically filled 
with customers's account on Yandex.Market information.

Dependencies

   * Drupal Commerce (http://drupal.org/project/commerce).

To install and configure

   1. Install and enable the module;
   2. Configure relationships between shipping profile fields 
   and Yandex profile at administration page 
   (admin/commerce/config/yandex-fast-order);
   3. Don't forget to add checkout url at Yandex fast order registration
   form (http://partner.market.yandex.ru/delivery-registration.xml);
   Refer to http://help.yandex.ru/partnermarket/?id=1121700 for more detail;
   4. ...
   5. Profit. Have a fun!
