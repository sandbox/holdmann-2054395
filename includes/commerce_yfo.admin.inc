<?php

/**
 * @file
 * Administrative callbacks and form builder functions
 * for Commerce Yandex fast order.
 */

/**
 * Commerce Yandex fast order admin form.
 */
function commerce_yfo_admin_form($form, &$form_state) {
  $button_images = array(
    'single_line' => theme('image', array(
      'path' => drupal_get_path('module', 'commerce_yfo') . '/images/button-ico-single-line.png',
      'alt' => 'Single line button',
    )),
    'double_lines' => theme('image', array(
      'path' => drupal_get_path('module', 'commerce_yfo') . '/images/button-ico-double-lines.png',
      'alt' => 'Double lines button',
    )),
  );
  $form['commerce_yfo_button_type'] = array(
    '#type' => 'radios',
    '#options' => $button_images,
    '#title' => t('Choose "Address from Yandex profile" button type'),
    '#default_value' => variable_get('commerce_yfo_button_type', 'single_line'),
    '#required' => TRUE,
  );

  $profile_data = commerce_yfo_profile_info();
  $profile_data_options = array();
  foreach ($profile_data as $field_name => $field_properties) {
    $profile_data_options[$field_name] = $field_properties['label'];
  }

  $instances = field_info_instances('commerce_customer_profile', 'billing');
  foreach ($instances as $field_name => $instance) {
    $field_info = field_info_field($instance['field_name']);
    if ($field_info['module'] != 'text') {
      continue;
    }
    $form['commerce_yfo_' . $field_name] = array(
      '#type' => 'select',
      '#title' => t('Field "!field_label (!field_name)" matches', array(
        '!field_label' => $instance['label'],
        '!field_name' => $field_name,
      )),
      '#options' => array('' => t('None')) + $profile_data_options,
      '#default_value' => variable_get('commerce_yfo_' . $field_name, ''),
    );
  }
  return system_settings_form($form);
}
