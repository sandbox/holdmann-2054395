<?php

/**
 * @file
 * Additional functions for Commerce Yandex fast order
 */

/**
 * Retrieve information about default Yandex fast order profile fields.
 */
function commerce_yfo_profile_info() {
  $profile_data = array(
    'street' => array(
      'label' => t('Street'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
    'building' => array(
      'label' => t('Building'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
    'suite' => array(
      'label' => t('Suite'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
    'flat' => array(
      'label' => t('Flat'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
    'entrance' => array(
      'label' => t('Entrance'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
    'floor' => array(
      'label' => t('Floor'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
    'intercom' => array(
      'label' => t('Intercom'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
    'city' => array(
      'label' => t('City'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
    'country' => array(
      'label' => t('Country'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
    'zip' => array(
      'label' => t('Zip'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
    'metro' => array(
      'label' => t('Metro'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
    'cargolift' => array(
      'label' => t('Cargolift'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
    'firstname' => array(
      'label' => t('First name'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
    'lastname' => array(
      'label' => t('Last name'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
    'fathersname' => array(
      'label' => t('Father name'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
    'phone' => array(
      'label' => t('Phone'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
    'phone-extra' => array(
      'label' => t('Extra phone'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
    'email' => array(
      'label' => t('Email'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
    'comment' => array(
      'label' => t('Comment to address'),
      'callback' => 'commerce_yfo_get_field_value',
    ),
  );

  $computed_data = array();
  foreach (module_implements('commerce_yfo_profile_info') as $module) {
    $module_callback = $module . '_commerce_yfo_profile_info';
    $computed_fields = $module_callback();
    $profile_data = array_merge($profile_data, $computed_fields);
  }
  return $profile_data;
}

/**
 * Callback for commerce_yfo_profile_info().
 */
function commerce_yfo_get_field_value($field, $data) {
  if (!isset($data[$field])) {
    return '';
  }
  return $data[$field];
}
